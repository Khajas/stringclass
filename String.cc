/*******************************
File: Source file for Assignment11
Submitted by: Khaja Anwar Ali Siddiqui
*******************************/
#include "String.h"
//Inclusion of header file.
const size_t String::npos=-1;
//Declaring the npos.
/******************************
*******************************/
String::String(){
    sz=1;		//Setting the size to 1 to hold the NULL character.
    p=new char[sz];	//Dynamically defining array of size 1.
    p[0]='\0';		//Assining the NUll character, this created an empty string.
}
//	The following member function accepts a string and makes the copy of the string passed.
//	It is usually referred as copy constructor.
String::String(const String& s){
    unsigned len=0;		//Reading the length of the string passed.
    for(len=0;s.p[len]!='\0';len++);	//len refers to the length of string passed.
    p=new char[len+1];	//Declaring a new array for the same length + 1 for NULL character.
    sz=len+1;		//setting the size to that of the new string size.
    for(unsigned i=0;i<sz;i++)
    p[i]=s.p[i];	//Copying the values to the string.
}
//      The following member function accepts a string, position and number of characters  and makes the copy of the string passed from postion for n characters
//      The new copy has the n  characters starting pos from the string s.
String::String(const String& s, const size_t& pos, const size_t& n){
    sz=n+1;		 //setting the size to that of the new string size.
    p=new char[sz];	//Declaring a new array for the same length + 1 for NULL character.
    for(unsigned i=pos,j=0; i<pos+n;i++,j++){
    p[j]=s.p[i];	//Copying the values to the string.
    }
    p[sz-1]='\0';	 //Assining the NUll character, this created an empty string.
}
//	The following constructor accepts a cstring and creates a cpp string untill N.
String::String(const char* cs, const size_t& n){    
    unsigned len;	//Reading the length of the string passed.
    for(len=0;cs[len]!='\0';len++);
    sz=n+1;		//setting the size to that of the new string size.
    p=new char[sz];	//Declaring a new array for the new string.
    for(unsigned i=0;(i<n&&i<sz-1);i++)
    p[i]=cs[i];		//Copying the values to the string.
    p[sz-1]='\0';	//Finally adding the NUll character.
}
//	The following constructor accepts a charater array and makes the String by appending NULl character at the end.
String::String(const char* cs){
    unsigned len;	//Reading the length of the string passed.
    for(len=0;cs[len]!='\0';len++);
    sz=len+1;		//setting the size to that of the new string size.
    p=new char[sz];	//Declaring a new array for the new string.
    for(unsigned i=0;i<sz-1;i++)
    p[i]=cs[i];		//Copying the values to the string.
    p[sz-1]='\0';	//Finally adding the NUll character.
}
//	The following constructor accepts the repeatition number n and character c, it makes a new string having n character of c.
String::String(const size_t& n, const char& c){
    sz=n+1;		//setting the size to that of the number of characters required.
    p=new char[sz];	//Declaring a new array for the new string with repeated characters untill c.
    for(unsigned i=0;i<n;i++)
    p[i]=c;		//Copying the values to the string.
    p[n]='\0';		//Finally adding the NUll character.
}
///////////////////////////////////////////////////////////////////////////////
//	Destructor, it simply deletes the memory allocated for the String object
String::~String(){
    delete [] p;	//Deleting the memory allocated to the array.
    sz=0;		//Setting the size to zero.
}			
///////////////////Overloading = operator	//////////////////////////
///	The following block of code overloads the = operator so that the string on LHS has same characters as that of the LHS.
///	Finally it returns the newly formed string.
String& String::operator=(const String& s){
    sz=s.sz;		//Setting the size to that of the string on LHS.
    delete [] p;	//Delering the contents of the string passed.
    p=new char[sz];	//Defining a new character array for the same size.
    for(unsigned i=0;i<sz-1;i++)
    p[i]=s.p[i];	//Copying the characters from string on LHS to the string on RHS.
    p[sz-1]='\0';	//Appending the NULL character.
    return *this;	//Finally Returning the address of current string object.
}
//	The following codes does the same operatore as that of the above, however it accepts a cstring.
String& String::operator=(const char* cs){
    unsigned len;
    for(len=0;cs[len]!='\0';len++);	//Reading the length of cstring.
    sz=len+1;			//Defining a new character array for the same size.
    delete[] p;
    p=new char[sz];		//Setting the size of the new array.
    for(unsigned i=0;i<(sz-1);i++)
    p[i]=cs[i];		//Copying the characters from string on LHS to the string on RHS.
    p[sz-1]='\0';	//Appending the NULL character.
    return *this;	//Finally Returning the address of current string object.
}
String& String::operator=(const char& c){
    sz = 2;		//Size should be two, one to hold the character and another for the NULL character.
    delete[] p;		//Deleting the current space for the srings.
    p = new char[sz];	//
    p[0] = c;
    p[sz -1] = '\0';
    return *this;
}
String& String::operator+=(const String& s){
    int len=s.sz+sz-1;
    char *temp=new char[len];
    for(unsigned i=0;i<sz-1;i++)  temp[i]=p[i];
    for(unsigned j=0;j<s.length();j++)   temp[j+sz-1]=s.p[j];
    delete [] p;
    sz=len;
    p= new char[sz];
    for(unsigned k=0;k<sz;k++) p[k]=temp[k];
    return *this;
}
String& String::operator+=(const char* cs){
    unsigned len;
    for(len=0;cs[len]!='\0';len++);
    char *temp=new char[sz+len];
    for(unsigned i=0;i<sz-1;i++) temp[i]=p[i];
    for(unsigned j=0;j<=len;j++) temp[sz-1+j]=cs[j];
    delete [] p;
    p= new char[sz+len];
    for(unsigned k=0;k<(sz+len);k++) p[k]=temp[k];
    sz=sz+len;
    return *this;
}
String& String::operator+=(const char& c){
    unsigned len=sz+1;
    char *temp=new char[len];
    for(unsigned i=0;i<sz-1;i++)    temp[i]=p[i];
    delete [] p;
    p= new char[len];
    sz=len;
    for(unsigned j=0;j<len-2;j++) p[j]=temp[j];
    p[len-2]=c;
    p[len-1]='\0';
    return *this;
}
size_t String::length() const{
    return sz-1;
}
size_t String::size() const{
    return sz-1;
}
bool String::empty() const{
    return (sz==1);
}
void String::clear(){
    delete [] p;
    sz=1;
    p=new char[1];
    p[0]='\0';
}
ostream& operator<< ( ostream& os, const String& s ){
    return os<<s.p;
}
//////////////////////////////////////////////////////////////r/////////////////////////////////////////////////////////////////////////////////
String& String::assign(const String& s){
	return *this=s;
}	
String& String::assign(const String& s, const size_t& pos, const size_t& n){
    String s1(s,pos,n);
    delete[] p;
    sz=s1.sz;
    p=new char[sz];
    for(unsigned i=0;i<sz;i++) p[i]=s1.p[i];
    return *this;
}
String& String::assign(const char*cs, const size_t& n){
    sz=n+1;
    p=new char[sz];
    for(unsigned i=0;i<sz-1;i++) p[i]=cs[i];
    p[sz-1]='\0';
    return *this;

}
String& String::assign(const char*cs){
    String s1(cs);
    return *this=s1;
}

String& String::assign(const size_t& n,const char c){
   String strr(n,c);
   return *this=strr;  
}

///////////        append()                  ////
String& String::append(const String& s){
    String strr;
    return *this=(strr+=s);
}
String& String::append(const String& s, const size_t& pos, const size_t& n){
    String strr(s,pos,n);
    return *this+=strr;
}
String& String::append(const char*cs, const size_t& n){
    String strr(cs,n);
    return *this+=strr;
}
String& String::append(const char* cs){
    return *this+=cs;
}
String& String::append(const size_t& n, const char& c){
    String s1(p);
    String s2(n,c);
    s1+=s2;
    delete[] p;
    sz=s1.sz;
    p= new char[sz];
    for(unsigned i=0;i<sz;i++) p[i]=s1.p[i];
    return *this;
}

///////////////            compare()            ////////////////////////////////////////////
int String::compare(const String& s)const{
        int v;
        bool gtFlag, ltFlag;
        bool eqFlag=gtFlag=ltFlag=true;
        if(sz!=s.sz) eqFlag=false;
        else for(unsigned i=0;i<s.length();i++){
        if(p[i]!=s.p[i]) eqFlag=false;
           }
        for(unsigned i=0,j=0;j<s.length();i++,j++){
        if(p[i]>=s.p[j]) ltFlag=false;
        }
        if(ltFlag||eqFlag) gtFlag=false;
        if(eqFlag) v=0;
        if(gtFlag) v=1;
        if(ltFlag) v=-1;   
        return v;
}
int String::compare(const char* cs)const{
    String s1(cs);
    return this->compare(s1);
}
int String::compare(const size_t& pos, const size_t& n, const String& s)const{
    String s1(p,pos,n);
    return s1.compare(s);
}
int String::compare ( const size_t& pos, const size_t& n, const char* cs ) const{
    String s1(p,pos,n);
    String s2(cs);
    return s1.compare(s2);
}
int String::compare(const size_t& pos1, const size_t& n1, const String& s, const size_t& pos2, const size_t& n2 )const{
    String s1(p,pos1,n1);
    String s2(s,pos2,n2);
    return s1.compare(s2);
}
int String::compare ( const size_t& pos, const size_t& n1, const char* cs, const size_t& n2 )const{
    String s1(p,pos,n1);
    String s2(cs,0,n2);
    return s1.compare(s2);
}
////////////////            insert ( ) function    ///////////////////////////////////////////
String& String::insert (const size_t& pos, const String& s){
    String s1(p);
    delete[] p;
    sz=s1.sz+s.sz-1;
    p=new char[sz];
    for(unsigned i=0;i<pos;i++) p[i]=s1.p[i];
    for(unsigned i=pos,j=0;j<s.sz-1;i++,j++) p[i]=s.p[j];
    for(unsigned i=pos+s.sz-1,j=pos;i<sz;j++,i++) p[i]=s1.p[j];
    return *this;
}
String& String::insert ( const size_t& pos1, const String& s, const size_t& pos2, const size_t& n ){
    String s1(s,pos2,n);
    String s2(p);
    *this=s2.insert(pos1,s1);
    return *this;
}
String& String::insert ( const size_t& pos, const char* cs, const size_t& n ){
    String s1(cs,n);
    String s2(p);
    return *this=s2.insert(pos,s1);
}
String& String::insert ( const size_t& pos, const char* cs ){
    String s1(cs);
    String s2(p);
    return *this=s2.insert(pos,s1);
}
String& String::insert ( const size_t& pos, const size_t& n, const char& c ){
    String s1(n,c);
    String s2(p);
    return *this=s2.insert(pos,s1);
}

/////////////////////               erase()             //////////////////////////
String& String::erase(const size_t& pos, const size_t& n){
    String s1(p);
    delete [] p;
    sz=s1.sz-n;
    p=new char[sz];
    for(unsigned i=0;i<pos;i++) p[i]=s1.p[i];
    for(unsigned i=pos,j=pos+n;j<s1.sz;j++,i++) p[i]=s1.p[j];
    return *this;
}
/////////////////////            Replace()              ///////////////////////////
String& String::replace ( const size_t& pos, const size_t& n, const String& s ){
    String s1(p);
    s1.erase(pos,n);
    s1.insert(pos,s);
    sz=s1.sz;
    delete [] p;
    p=new char[sz];
    for(unsigned i=0;i<sz;i++) p[i]=s1.p[i];
    return *this;
}
String& String::replace( const size_t& pos1, const size_t& n1, const String& s, const size_t& pos2, const size_t& n2 ){
    String s1(p);
    String s2(s,pos2,n2);
    s1.erase(pos1,n1);
    s1.insert(pos1,s2);
    sz=s1.sz;
    delete [] p;
    p= new char[sz];
    for(unsigned i=0;i<sz;i++) p[i]=s1.p[i];
    return *this;
}
String& String::replace ( const size_t& pos, const size_t& n1, const char* cs, const size_t& n2 ){
    String s1(p);
    String s2(cs,n2);
    s1.erase(pos,n1);
    s1.insert(pos,s2);
    sz=s1.sz;
    delete [] p;
    p= new char[sz];
    for(unsigned i=0;i<sz;i++) p[i]=s1.p[i];
    return *this;
}
String& String::replace( const size_t& pos, const size_t& n, const char* cs ){
    String s1(p);
    String s2(cs);
    s1.replace(pos,n,s2);
    sz=s1.sz;
    delete [] p;
    p= new char[sz];
    for(unsigned i=0;i<sz;i++) p[i]=s1.p[i];
    return *this;
}
String& String::replace ( const size_t& pos, const size_t& n1, const size_t& n2, const char& c ){
    String s1(p);
    String s2(n2,c);
    s1.replace(pos,n1,s2);
    sz=s1.sz;
    delete [] p;
    p= new char[sz];
    for(unsigned i=0;i<sz;i++) p[i]=s1.p[i];
    return *this;
}
/////////           copy()                  ////////////////////////
size_t String::copy(char* cs,const size_t& n,const size_t& pos)const{
    size_t i,j;
    for(i=pos,j=0;j<n;j++,i++) cs[j]=p[i];
    return j;
}
////////            void swap ( String & s )       /////////////////
void String::swap(String & s){
    const size_t bufSize = sz; char buffer [bufSize];
    size_t len = this->copy (buffer,sz-1,0); buffer [ len ] = 0;
    *this=s;
    s=buffer;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
